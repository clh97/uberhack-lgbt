// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js');

var config = {
  apiKey: "AIzaSyAKxNSSAGveJDhzLZrjhMBZRyy68yKm2w8",
  authDomain: "uber-hack-lgbt.firebaseapp.com",
  databaseURL: "https://uber-hack-lgbt.firebaseio.com",
  projectId: "uber-hack-lgbt",
  storageBucket: "uber-hack-lgbt.appspot.com",
  messagingSenderId: "43750649167"
};

firebase.initializeApp(config);