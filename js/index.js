const messaging = firebase.messaging();

messaging.usePublicVapidKey("BPIeBivLz4ugFt4MfAyyr2UFLOOC5S4bCcXAk0LHxl8KKiXO6OpnozRBXfIjgDDJ7nuHT0ylXgncplTOZGkn1HI");

messaging.requestPermission()
  .then(() => {
    console.log('permissão de notificações concedida :)')
    return messaging.getToken();
  })
  .then(t => console.log(t))
  .catch(e => {
    console.log(e)
    console.log('não poderemos enviar notificações sem sua permissão')
  })

messaging.onMessage(payload => {
  console.log(`onMessage: ${payload}`)
})

const authentication = {
  email: 'bla@bla.com',
  password: '123456',
}

authenticate(authentication).then(console.log)


/**
 * Faz a autenticação do usuário no servidor e retorna seu token de acesso.
 * @param {AuthenticateParams} params
 * @returns {Promise.<string>}
 */
async function authenticate (params) {
  const API = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAKxNSSAGveJDhzLZrjhMBZRyy68yKm2w8`;

  try {
    const response = await fetch(API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: params.email,
        password: params.password,
        returnSecureToken: true
      }),
    });

    /**
     * @type {AuthenticateResponse}
     */
    const { idToken: token } = await response.json();

    return token;
  } catch (error) {
    console.dir(error);
  }
}
