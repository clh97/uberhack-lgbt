/**
 * Faz a autenticação do usuário no servidor e retorna seu token de acesso.
 * @param {AuthenticateParams} params
 * @returns {Promise.<string>}
 */
async function authenticate (params) {
  const API = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAKxNSSAGveJDhzLZrjhMBZRyy68yKm2w8`;

  try {
    const response = await fetch(API, {
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: params.email,
        password: params.password,
        returnSecureToken: true
      }),
    });

    /**
     * @type {AuthenticateResponse}
     */
    const { idToken: token } = await response.json();

    return token;
  } catch (error) {
    console.dir(error);
  }
}

